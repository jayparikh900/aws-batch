package com.info.demo.aws_batch;

import java.io.File;
import java.util.UUID;

import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.stereotype.Component;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;

@Component
public class BatchListener extends JobExecutionListenerSupport {

	@Override
	public void afterJob(JobExecution jobExecution) {
		if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
			try {
				System.out.println("Job Complete >>>>>>>>>>>");

				AWSCredentials credentials = new BasicAWSCredentials("AKIAIGIMJT3GJ5UFUWBA",
						"sEFK+m7tmTL0Py7SVTRcwRcfHa05RdSgIPwGkUX9");
				AmazonS3 s3 = new AmazonS3Client(credentials);

				String bucketName = "batch-demo-" + UUID.randomUUID();
				String key = "output.csv";

				System.out.println("Creating bucket " + bucketName + "\n");
				s3.createBucket(bucketName);

				File file = new File("./output.csv");
				if (file.exists()) {
					System.out.println("Uploading a new object to S3 from a file\n");
					s3.putObject(new PutObjectRequest(bucketName, key, file));
				} else {
					System.out.println("File Not Found Exception");
				}

			} catch (Exception ase) {
				System.out.println("Exception Occur >>> ");
			}

		}
	}
}
