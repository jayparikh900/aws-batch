package com.info.demo.aws_batch;

import org.springframework.batch.item.ItemProcessor;

public class UserItemProcessor implements ItemProcessor<User, User>{

	@Override
	public User process(User user) throws Exception {
		if(user.getDept() != null) {
			user.setDept(user.getDept().toUpperCase());
		}
		return user;
	}

}
