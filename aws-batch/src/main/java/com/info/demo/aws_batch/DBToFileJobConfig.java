package com.info.demo.aws_batch;


import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.MapJobRepositoryFactoryBean;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.batch.item.file.transform.FieldExtractor;
import org.springframework.batch.item.file.transform.LineAggregator;
import org.springframework.batch.support.transaction.ResourcelessTransactionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.transaction.PlatformTransactionManager;

import com.mysql.jdbc.Driver;

@Configuration
@EnableBatchProcessing
public class DBToFileJobConfig {
	
	@Autowired
	JobBuilderFactory jobBuilderFactory;
	
	@Autowired
	StepBuilderFactory stepBuilderFactory;
	
	
	
	@Bean
    public Job job(Step step, JobRepository jobRepository, BatchListener batchListner) {
		return jobBuilderFactory.get("job-"+System.currentTimeMillis()).start(step).listener(batchListner).build();
    }
	
	@Bean
    public ItemProcessor<User, User> processor() {
        return new UserItemProcessor();
    }
	
	@Bean
	public PlatformTransactionManager getTransactionManager() {
	    return new ResourcelessTransactionManager();
	}

	@Bean
	public JobRepository getJobRepo() throws Exception {
	    return new MapJobRepositoryFactoryBean(getTransactionManager()).getObject();
	}

	@Bean
    protected Step step(ItemReader<User> databaseItemReader, ItemProcessor<User, User> processor, ItemWriter<User> csvFileItemWriter) {
        return stepBuilderFactory.get("step")
            .<User, User> chunk(10)
            .reader(databaseItemReader)
            .processor(processor)
            .writer(csvFileItemWriter)
            .build();
    }

	@Bean
	ItemReader<User> databaseItemReader() throws SQLException {
		String QUERY_FIND_USER_INFO = "select * from user_info";
		
		JdbcCursorItemReader<User> databaseReader = new JdbcCursorItemReader<User>();
		databaseReader.setDataSource(getClientDataSource());
		databaseReader.setSql(QUERY_FIND_USER_INFO);
		databaseReader.setRowMapper(new BeanPropertyRowMapper<User>(User.class));
		return databaseReader;
	}

	
	@Bean
    public ItemWriter<User> csvFileItemWriter() {
		FlatFileItemWriter<User> csvFileWriter = new FlatFileItemWriter<User>();
		csvFileWriter.setResource(new FileSystemResource("./output.csv"));
		
		LineAggregator<User> lineAggregator = createUserLineAggregator();
        csvFileWriter.setLineAggregator(lineAggregator);
		return csvFileWriter;
    }
	
	private DataSource getClientDataSource() throws SQLException {
		SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
        dataSource.setUrl(Constants.READER_DB_URL);
        dataSource.setUsername(Constants.READER_DB_USERNAME);
        dataSource.setPassword(Constants.READER_DB_PASSWORD);
        dataSource.setDriver(new Driver());
        return dataSource;
	}
	
	
	private LineAggregator<User> createUserLineAggregator() {
        DelimitedLineAggregator<User> lineAggregator = new DelimitedLineAggregator<User>();
        lineAggregator.setDelimiter(",");
 
        FieldExtractor<User> fieldExtractor = createUserFieldExtractor();
        lineAggregator.setFieldExtractor(fieldExtractor);
 
        return lineAggregator;
    }
	
	private FieldExtractor<User> createUserFieldExtractor() {
        BeanWrapperFieldExtractor<User> extractor = new BeanWrapperFieldExtractor<User>();
        extractor.setNames(new String[] {"name", "dept", "salary"});
        return extractor;
    }
	
}
