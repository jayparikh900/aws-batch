This is Maven based spring boot project. So you may run this using spring boot run command.
You need to do below changes to run project properly.
- Give database configuration in application.properties file which is used by spring batch to store batch metadata.
- Change database credentials in Constant.java file from which you want to read data set.
- Change query for database Item reader in method "databaseItemReader" of DBToFileJobConfiguration file as per your requirement
  and also create test data in that table.
- When you run program so output.csv file will generate in main directory and it will be uploaded to S3 bucket of given AWS account.  